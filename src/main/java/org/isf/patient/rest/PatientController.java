package org.isf.patient.rest;

import java.util.ArrayList;
import java.util.List;

import org.isf.patient.dto.PatientDTO;
import org.isf.patient.manager.PatientBrowserManager;
import org.isf.patient.mapper.PatientMapper;
import org.isf.patient.model.Patient;
import org.isf.shared.exceptions.OHAPIException;
import org.isf.utils.exception.OHServiceException;
import org.isf.utils.exception.model.OHExceptionMessage;
import org.isf.utils.exception.model.OHSeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;

@RestController
@Api(value="/patients",produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization(value="basicAuth")})
public class PatientController {

	protected static final String DEFAULT_PAGE_SIZE = "80";

    @Autowired
	protected PatientBrowserManager patientManager;
    @Autowired
	protected PatientMapper patientMapper;

	private final Logger logger = LoggerFactory.getLogger(PatientController.class);

	public PatientController(PatientBrowserManager patientManager, PatientMapper patientMapper) {
		this.patientManager = patientManager;
		this.patientMapper = patientMapper;
	}

    /**
     * Create new Patient
     * @param newPatient
     * @return
     * @throws OHServiceException
     */
	@PostMapping(value = "/patients", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Integer> newPatient(@RequestBody PatientDTO newPatient) throws OHServiceException {
        String name = StringUtils.isEmpty(newPatient.getName()) ? newPatient.getFirstName() + " " + newPatient.getSecondName() : newPatient.getName();
        logger.info("Create patient "  + name);
        Patient patient = patientManager.savePatient(patientMapper.map2Model(newPatient));
        if(patient == null){
            throw new OHAPIException(new OHExceptionMessage(null, "Patient is not created!", OHSeverityLevel.ERROR));
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(patient.getCode());
	}

	@PutMapping(value = "/patients/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Integer> updatePatient(@PathVariable int code, @RequestBody PatientDTO updatePatient) throws OHServiceException {
        logger.info("Update patient code:"  +  code);
        Patient patient = patientMapper.map2Model(updatePatient);
        patient.setCode(code);
        patient = patientManager.savePatient(patient);
        if(patient.getCode() == code){
            throw new OHAPIException(new OHExceptionMessage(null, "Patient is not updated!", OHSeverityLevel.ERROR));
        }
        return ResponseEntity.ok(patient.getCode());
	}

	@GetMapping(value = "/patients", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PatientDTO>> getPatients(
			@RequestParam(value="page", required=false, defaultValue="0") Integer page,
			@RequestParam(value="size", required=false, defaultValue=DEFAULT_PAGE_SIZE) Integer size) throws OHServiceException {
        logger.info("Get patients page:"  +  page + " size:" + size);
	    ArrayList<Patient> patients = patientManager.getPatient(page, size);
        List<PatientDTO> patientDTOS = patientMapper.map2DTOList(patients);
        if(patientDTOS.size() == 0){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(patientDTOS);
        }else{
            return ResponseEntity.ok(patientDTOS);
        }
	}

	@GetMapping(value = "/patients/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PatientDTO> getPatient(@PathVariable Integer code) throws OHServiceException {
        logger.info("Get patient code:"  +  code);
		Patient patient = patientManager.getPatientById(code);
		if (patient == null) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		return ResponseEntity.ok(patientMapper.map2DTO(patient));
	}


	@GetMapping(value = "/patients/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PatientDTO> searchPatient(
			@RequestParam(value="name", defaultValue="") String name,
			@RequestParam(value="code", required=false) Integer code) throws OHServiceException {
        logger.info("Search patient name:" + name + " code:"  +  code);
		Patient patient = null;
		if(code != null) {
            patient = patientManager.getPatientById(code);
		}else if (!name.equals("")) {
            patient = patientManager.getPatientByName(name);
		}
        if (patient == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
        return ResponseEntity.ok(patientMapper.map2DTO(patient));
	}
	
	@GetMapping(value = "/patients/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PatientDTO> getPatientAll(@RequestParam Integer code) throws OHServiceException {
        logger.info("get patient for provided code even if logically deleted: " + code);
        Patient patient = patientManager.getPatientAll(code);
        if (patient == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
        return ResponseEntity.ok(patientMapper.map2DTO(patient));
	}
	
	@GetMapping(value = "/patients/nextcode", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> getPatientNextCode() throws OHServiceException {
        logger.info("get patient next code");
        int nextCode = patientManager.getNextPatientCode();
        return ResponseEntity.ok(nextCode);
	}

	@DeleteMapping(value = "/patients/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deletePatient(@PathVariable int code) throws OHServiceException {
        logger.info("Delete patient code:"  +  code);
        Patient patient = patientManager.getPatientById(code);
        boolean isDeleted = false;
        if (patient != null) {
            isDeleted = patientManager.deletePatient(patient);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        if (!isDeleted) {
            throw new OHAPIException(new OHExceptionMessage(null, "Patient is not deleted!", OHSeverityLevel.ERROR));
        }
        return ResponseEntity.ok(isDeleted);
    }
	
	@GetMapping(value = "/patients/merge", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> mergePatients(@RequestParam Integer mergedcode, @RequestParam Integer code2) throws OHServiceException {
        logger.info("merge patient for code " + code2 + " in patient for code " + mergedcode);
        Patient mergedPatient = patientManager.getPatientById(mergedcode);
        Patient patient2 = patientManager.getPatientById(code2);
        if(mergedPatient == null || patient2 == null) {
        	return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        boolean merged = patientManager.mergePatient(mergedPatient, patient2);
        if(!merged) {
        	throw new OHAPIException(new OHExceptionMessage(null, "Patients are not merged!", OHSeverityLevel.ERROR));
        }
        return ResponseEntity.ok(merged);
	}
}
