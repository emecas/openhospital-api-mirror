package org.isf.testing.rest;

import org.isf.accounting.rest.BillControllerTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ControllerBaseTest {

	protected final Logger logger = LoggerFactory.getLogger(BillControllerTest.class);

	public ControllerBaseTest() {
		super();
	}

}